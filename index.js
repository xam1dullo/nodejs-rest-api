const express = require('express')
const app = express()
const mongoose = require('mongoose')
const dotenv = require('dotenv')
const helmet = require('helmet')
const morgan = require('morgan')


const userRoute =  require('./routes/users.js');
const authRoute =  require('./routes/auth.js');
dotenv.config()

mongoose.connect(
   process.env.MONGO_URL,
   { useNewUrlParser: true, useUnifiedTopology: true },
   () => {
      console.log('Connected to MongoDB');
   }
);
const port =8080

//middleware
app.use(express.json())
app.use(express.urlencoded())
app.use(helmet());
app.use(morgan('common'))


app.use('/api/users', userRoute)

app.use('/api/auth',authRoute)


app.listen(port, () => console.log(`Backend server in running on port ${port}`))





// const { MongoClient } = require('mongodb');
// const uri = "mongodb+srv://xam1dullo:<password>@nodejsresapi.2mruy.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
// const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
// client.connect(err => {
//   const collection = client.db("test").collection("devices");
//   // perform actions on the collection object
//   client.close();
// });
